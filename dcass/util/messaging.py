#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  messaging.py
#  
#  Copyright 2013 Per Karlsson, pkarl.home@gmail.com
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#
# Some code borrowed from and adapted from agocontrol, especially id conversions
# and device registration functions.


import connectionmanager.cmclient as cmclient
import simplejson as json
import uuid
import configparser

class Messenger(object):
    """TODO: Make device register callbacks for all the commands that it can handle,
    in such a way that if a command comes in here that is not supported, we should raise
    error in some suitable way, perhaps emit event.error.commandnotsupported?
    Also, there must be a way to translate UUID of all the devices this device controls
    to a local id, since if there is more than one instance of this particular device, the uuid 
    will not be the same for childdevices. Perhaps we should let devicemanager look this up and 
    send it to us at our request?
    
    We should keep a list of all commands in the system and allow devices to register for them,
    in this way the same messenger object can and should be used by all devices no matter what
    they are or do."""


    def __init__(self, my_id, cm_address):
        self.my_id = my_id
        self.cm_address = cm_address
        self.cm = cmclient.Cmclient(my_id, cm_address)
        self.uuids = {}
        self.handler = None
        self.eventhandler = None
        self.load_id_conversion_map()
        self.devices = {}

    def connect(self):
        self.cm.connect_to_cm()
        self.cm.join_channel(str(self.my_id))
        return True
    
    def join_channel(self, channel):
        self.cm.join_channel(channel)
    
    def leave_channel(self, channel):
        self.cm.leave_channel(channel)

    def send_command(self, command, sender_id, to_device_id, child_device_id):

        """Make up a channel to get the response. If this channel is made up
        here and now, there is virtually zero chance that any message that we get
        from this channel is NOT a reply to our message."""
        message = {}
        message["from_uuid"] = str(sender_id)
        message["to_uuid"] = str(to_device_id)
        message["ack"] = True
        message["subject"] = "command"
        content = {}
        content["device_uuid"] = str(child_device_id)
        content["command"] = command
        reply_channel = str(uuid.uuid4())
        message["reply_to"] = reply_channel
        """Now we have to join this channel to actually get the reply"""
        self.cm.join_channel(reply_channel)
        message["content"] = content
        message = json.dumps(message)
        self.cm.send_message_to_client(str(to_device_id), str(sender_id),message)
        message = self.get_channel_message(reply_channel)
        if message != None:
            self.cm.leave_channel(reply_channel)
            content = message["content"]
            return content['result']
        else:
            return 'timeout'
           
    def send_command_reply(self, sender_id, to_device_id, reply_channel, result):
        
        content = {}
        content["result"] = result
        message = {}
        message["from_uuid"] = str(sender_id)
        message["to_uuid"] = str(to_device_id)
        message["ack"] = False
        message["subject"] = "reply"
        message["content"] = content        
        message = json.dumps(message)
        self.cm.send_message_to_channel(str(reply_channel), message, sender_id)
        
    
    def get_message(self):
        message = self.cm.get_message()
        if message != None:
            try:
                message = json.loads(message)
                return message
            except:
                pass

    def get_message_count(self):
        message_count = self.cm.get_message_count()
        return message_count
    
    
    def get_channel_message(self, channel):
        message = self.cm.get_channel_message(channel)
        if message != None:
            try:
                message = json.loads(message)
                return message
            except:
                return None
        else:
            return None

    def get_channel_message_count(self, channel):
        message_count = self.cm.get_channel_message_count(channel)
        return message_count    
    
    
    
    def save_id_conversion_map(self):
        with open('/etc/dcass/id_conversions/id_conversion_map' + self.my_id + '.json' , 'w') as outfile:
            json.dump(self.uuids, outfile)

    def load_id_conversion_map(self):
        try:
            with open('/etc/dcass/id_conversions/id_conversion_map' + self.my_id + '.json' , 'r') as infile:
                self.uuids = json.load(infile)
        except:
            pass
    
    def internal_id_to_uuid(self, internalid):
        for uuid in self.uuids:
            if (self.uuids[uuid] == internalid):
                return uuid

    def uuid_to_internal_id(self, uuid):
        try:
            return self.uuids[uuid]
        except KeyError:
            return None
    
    def get_config_option(self, device_id, option, default):
        config = configparser.ConfigParser()
        try:
            config.read('/etc/dcass/device_config/' + device_id + '.conf')
            config_value = config.get(device_id,option)
        except:
            config_value = default
        return config_value    

    
    
    def register_command_handler(self, command_handler):
        self.command_handler = command_handler
        
    def register_event_handler(self, event_handler):
        self.event_handler = event_handler

        
    def add_device(self, internalid, devicetype):
        if (self.internal_id_to_uuid(internalid) == None):
            self.uuids[str(uuid.uuid4())]=internalid
            self.save_id_conversion_map()
        device = {}
        device["devicetype"] = devicetype
        device["internalid"] = internalid
        self.devices[self.internal_id_to_uuid(internalid)] = device
        self.send_device_announce(self.internal_id_to_uuid(internalid), device)

    def remove_device(self, internalid):
        if (self.internal_id_to_uuid(internalid) != None):    
            self.send_device_remove(self.internal_id_to_uuid(internalid))
            del self.devices[self.internal_id_to_uuid(internalid)]
    
    def send_device_announce(self, uuid, device):
        content = {}
        content["devicetype"]  = device["devicetype"]
        content["uuid"]  = uuid
        content["internalid"]  = device["internalid"]
        content["handled-by"]  = self.my_id
        message = {}
        message["from_uuid"] = self.my_id
        message["to_uuid"] = ''
        message["ack"] = False
        message["subject"] = "event.device.announce"
        message["content"] = content        
        message = json.dumps(message)
        self.cm.send_message_to_channel('events', message, self.my_id)            

    def send_device_remove(self, uuid):
        content = {}
        content["uuid"]  = uuid
        message = {}
        message["from_uuid"] = self.my_id
        message["to_uuid"] = ''
        message["ack"] = False
        message["subject"] = "event.device.remove"
        message["content"] = content        
        message = json.dumps(message)
        self.cm.send_message_to_channel('events', message, self.my_id)            

    def send_event(self, internalId, eventType, level, unit):
        content  = {}
        content["uuid"]=self.internal_id_to_uuid(internalId)
        content["level"]=level
        content["unit"]=unit
        message = {}
        message["from_uuid"] = self.my_id
        message["to_uuid"] = ''
        message["ack"] = False
        message["subject"] = eventType
        message["content"] = content        
        message = json.dumps(message)
        self.cm.send_message_to_channel('events', message, self.my_id)

    def send_lookup(self, child_device):
        content  = {}
        content["uuid"]=child_device
        message = {}
        message["from_uuid"] = self.my_id
        message["to_uuid"] = ''
        message["ack"] = True
        message["subject"] = 'command.device.lookup'
        message["content"] = content        
        message = json.dumps(message)
        self.cm.send_message_to_channel('devicemanager', message, self.my_id)
    
    def report_devices(self):
        
        for device in self.devices:
            print('Reporting device:', device, self.devices[device])
            self.send_device_announce(device, self.devices[device])
    
    def run(self):
        
        while True:
            msg = self.get_message()
            if msg != None:
                subject = msg["subject"]
                content = msg["content"]
                if "to_uuid" in msg and subject == "command":
                    #print('got command')
                    if content['device_uuid'] in self.devices:
                        myid = self.uuid_to_internal_id(content["device_uuid"])
                        if myid != None and self.command_handler:
                            result = self.command_handler(myid, content)
                            from_uuid = msg["from_uuid"]
                            reply_to = msg["reply_to"]
                            if msg["ack"] == True:
                                self.send_command_reply(self.my_id, from_uuid, reply_to, result)
            

            

