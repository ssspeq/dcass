#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  cli.py
#  
#  Copyright 2013 Per Karlsson, pkarl.home@gmail.com
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#

import argparse
import util.messaging
import uuid
import timeit
import socket
import os
if os.name != "nt":
    import fcntl
    import struct
"""A commandline interface to DCASS"""

parser=argparse.ArgumentParser()
#parser.add_argument('-a', help = 'Network address, X.X.X.X', required=True)
parser.add_argument('-c', help = 'Command to send')
parser.add_argument('-t', help = 'target device')
parser.add_argument('-d', help = 'child device')
parser.add_argument('-rc', action='store_true', help = 'Send command by devicemanager lookup')
parser.add_argument('-speed', action='store_true', help = 'Speed test, testdevice must be running')
parser.add_argument('-rawspeed', action='store_true', help = 'Speed test, no reply')

args=parser.parse_args()
my_id = uuid.UUID('2658243b-b264-4359-9fc8-dbf16d9c83a5')
my_reply_channel = uuid.uuid4()
command = args.c
target_device = args.t
child_device = args.d

def main():

    print('Assuming', cm_address,'is what you want to use')
    if messenger.connect() == True:
        if args.speed == True:
            t = timeit.Timer(stmt=send_command_test)
            time = t.timeit(number=1)
            print('10000 messages sent and acknowledged from target in:\n'+str(time),
                  'seconds','\n'+'at a message rate of:\n'+str((10000/time)*2),'messages/second')
        elif args.rawspeed == True:
            t = timeit.Timer(stmt=speed_test)
            time = t.timeit(number=1)
            print('10000 messages sent in:\n'+str(time),
                  'seconds','\n'+'at a message rate of:\n'+str(10000/time),'messages/second')        
        
        elif args.rc == True:
            messenger.send_lookup(args.t)
            response = messenger.get_message()
            print(response)
        
        else:
            send_command()

def send_command_test():
    for i in range(10000):
        if messenger.send_command(command, my_id, target_device, child_device) == False:
            break
        else:
            continue
    print('ok')

def send_command():
 
    result = messenger.send_command(command, my_id, target_device, child_device)
    messenger.leave_channel(str(my_id))
    print('Command result:',  result)
              
def speed_test():
    for i in range(10000):
        messenger.send_command(command, my_id, my_id)
    print('ok')    

def get_interface_ip(ifname):
        ifname = ifname.encode('utf8')
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        #value = value.encode(encoding='utf_8', errors='strict')
        return socket.inet_ntoa(fcntl.ioctl(s.fileno(), 0x8915, struct.pack(b'256s',
                                ifname[:15]))[20:24])

def get_lan_ip():
    ip = socket.gethostbyname(socket.gethostname())
    if ip.startswith("127.") and os.name != "nt":
        interfaces = [
            "eth0",
            "eth1",
            "eth2",
            "wlan0",
            "wlan1",
            "wifi0",
            "ath0",
            "ath1",
            "ppp0",
            ]
        for ifname in interfaces:
            try:
                ip = get_interface_ip(ifname)
                break
            except IOError:
                pass
    return ip

cm_address = get_lan_ip()
messenger = util.messaging.Messenger(str(my_id), str(cm_address))

if __name__ == '__main__':
    main()
