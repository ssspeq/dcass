#!/bin/bash
if [[ $(/usr/bin/id -u) -ne 0 ]]; then
    echo "You must run this with superuser privileges!"
    exit
fi
file="/etc/dcass/dcass.conf"
if [ -f "$file" ]
then
	echo "Main dcass config already exists."
else
	echo "Installing dcass config."
	mkdir -p /etc/dcass
	cp ./config/dcass_main/dcass.conf /etc/dcass/dcass.conf
fi
mkdir -p /etc/dcass/id_conversions
chmod -R ugo+rw /etc/dcass/id_conversions
mkdir -p /etc/dcass/device_config
chmod -R ugo+rw /etc/dcass/device_config
mkdir -p /etc/dcass/devices
chmod -R ugo+rw /etc/dcass/devices
echo "Done!"
