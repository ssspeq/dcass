#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  connectionmanager.py
#  
#  Copyright 2013 Per Karlsson, pkarl.home@gmail.com
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import threading
import socket
import connectionmanager.node as node
import constants.constants as constants
import util.tools as tools
import connectionmanager.beacon as beacon
import select
import queue
import simplejson as json
import re
import time
import configparser
s = None
run = True
import uuid

nodes = {}

outgoing_message_queues = {}#To hold messages to be sent
add_to_epoll_queue = queue.Queue()#To temporarily store new nodes that were discovered



class ConnectionManager: 


    def __init__(self,my_address,my_network_address, DEBUG):
        constants.DEBUG = DEBUG
        self.my_port = 4501
        self.my_address = my_address
        self.serversocket = None
        self.epoll = select.epoll()
        self.my_id = uuid.uuid4()
        self.my_network_address = my_network_address
        
        testbeacon = beacon.Beacon(str(self.my_id),str(self.my_port),my_network_address,my_address)
        testbeacon.run()
        
    def run(self):
        #t1 = threading.Thread(target=self.socket_handler);
        #t1.daemon = True
        #t1.start()
        
        t2 = threading.Thread(target=self.beacon_listener, args=(self.my_id,self.my_address));
        t2.daemon = True
        t2.start()
        self.socket_handler()

    def socket_handler(self):
        HOST, PORT = self.my_address, self.my_port
        self.serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.serversocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.serversocket.setsockopt(socket.SOL_TCP, socket.TCP_NODELAY, 1)
        self.serversocket.bind((HOST, PORT))
        self.serversocket.listen(10)
        self.my_node = node.Node(self.my_id, self.my_address)
        self.connections = {}
        self.epoll.register(self.serversocket.fileno(), select.EPOLLIN)
        
        while run:
                events = self.epoll.poll(0.001)
                for fileno, event in events:
             
                    #New connection
                    if fileno == self.serversocket.fileno():
                        self.setup_incoming_connection()
                    #New incoming data
                    elif event & select.EPOLLIN:
                        new_socket = self.connections[fileno]
                        try:
                            data = tools.recv_msg(new_socket)
                            if data != None and data != '':
                                """This is where we need to start intercepting
                                messages that are not for the incoming queue, but for
                                other things such as topics/channel notifications"""
                                
                                self.handle_incoming_message(data, new_socket)
                                
                            else:
                                """This socket is broken and will never ever work again
                                so clean up"""
                                if constants.DEBUG:print('socket error, trying to clean up')
                                self.delete_connection(new_socket, fileno)
                        except Exception as e:
                            print('Connectionmanager: Error in init:',e)

                    
                    #some socket is writable
                    elif event & select.EPOLLOUT:
                        new_socket = self.connections[fileno]
                        try:
                            next_msg = outgoing_message_queues[new_socket].get_nowait()
                        except queue.Empty:
                            
                            for connection in self.connections:
                                self.epoll.modify(self.connections[connection], select.EPOLLERR |
                                                  select.EPOLLHUP |
                                                  select.EPOLLIN)
                                """Make epoll stop nagging about this fileno beeing writable
                                because the queue is empty so we don't care."""
                            
                        else:
                            if constants.DEBUG:print ('sending "%s" to %s' % (next_msg, new_socket.getpeername()))
                            tools.send_msg(new_socket, next_msg.encode('utf8'))

                    elif event & (select.EPOLLERR | select.EPOLLHUP):
                        new_socket = self.connections[fileno]
                        self.delete_connection(new_socket, fileno)
               
                    for sock in outgoing_message_queues:
                        try:
                            next_msg = outgoing_message_queues[sock].get_nowait()
                            
                            for connection in self.connections:
                                self.epoll.modify(self.connections[connection],
                                                  select.EPOLLIN |
                                                  select.EPOLLOUT |
                                                  select.EPOLLERR |
                                                  select.EPOLLHUP)
                            
                            outgoing_message_queues[sock].put_nowait(next_msg)
                        except queue.Empty:
                            pass
                self.add_and_setup_discovered_node()




       


        
    def send_message_to_all_connected_nodes_internal_use(self, message):
        """TODO: Fix this, its now sending everything to clients aswell, this is not what we want."""
        message = message
        for node_id, node_obj in nodes.items():
            outgoing_message_queues[node_obj.getConnectionSocket()].put_nowait(message)


    
    def delete_connection(self, new_socket, fileno):
        """This deletes a node and or client completely, and removes every membership of channels it
        may have had. """
        
        
        for node_id, node_obj in nodes.copy().items():
            
            if node_obj.getConnectionSocket() == new_socket:
                print('node fd', new_socket, fileno)
                self.unregister_epoll(new_socket, fileno)
                """All clients belonging to this node should now also be gone and
                unregistered"""
                    
                nodes.pop(node_id, None)
                if constants.DEBUG:print('Connectionmanager: removed node', str(node_id))
                
                
        
        """If the connection we lost was a client, we must remove it, and tell everyone
        that this client no longer lives. Then, we will not get messages to any channel
        the client subscribed to."""    
        clients = self.my_node.getClients()
        for client_id, client_obj in clients.copy().items():
            client_socket = client_obj.getSocket()
            if client_socket is new_socket:
                """Remove client from our list"""
                self.my_node.removeClient(client_id)
                self.unregister_epoll(client_socket, client_socket.fileno())
                if constants.DEBUG:print('Connectionmanager: removed client', str(client_id))
                message = {u"from": str(self.my_id), u"to": 'broadcast', 
                           u"message_type": 'update.client.rip', u"client": client_id}
                message = json.dumps(message)
                self.send_message_to_all_connected_nodes_internal_use(message)
    
    
    
    def unregister_epoll(self, socket, fileno):
        socket = socket
        fileno = fileno
        
        self.epoll.unregister(fileno)
        if fileno != -1:
            self.connections[fileno].close()
        del self.connections[fileno]
        del outgoing_message_queues[socket]

    
    def add_and_setup_discovered_node(self):
        """This is used by epoll to setup a new node that was discovered by beaconlistener"""
        
        try:
            new_node = add_to_epoll_queue.get_nowait()
        except queue.Empty:
            #TODO: Fix this to handle the case where the queue is full of unadded nodes, it will
            #probably never happen but still
            pass
        else:
            if constants.DEBUG:print('Connectionmanager: Found new node ',new_node.getNodeId())
                    
            newsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            newsock.connect((new_node.getRemoteAddress(), int(new_node.getRemotePort())))
            newsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            newsock.setsockopt(socket.SOL_TCP, socket.TCP_NODELAY, 1)
            message = ('node '+str(self.my_id)).encode('utf8')
            tools.send_msg(newsock, message)
            response = tools.recv_msg(newsock)
            
            if response.decode('utf8') == 'OK':
                if constants.DEBUG:print('Connectionmanager: Connected to node'+str(new_node.getNodeId()))
                
                newsock.setblocking(0)
                new_node.setConnectionSocket(newsock)
                new_node_id = new_node.getNodeId()
                nodes.update({new_node_id:new_node})
                outgoing_message_queues[newsock] = queue.Queue()
                self.epoll.register(newsock.fileno(), select.EPOLLIN | select.EPOLLOUT | select.POLLERR | select.EPOLLHUP)
                self.connections[newsock.fileno()] = newsock
                self.send_all_clients(newsock, new_node_id)
                if constants.DEBUG:print ("Connectionmanager: New node object with id:"+str(new_node.getNodeId())+" and remote_address:"+new_node.getRemoteAddress())
            else:
                if constants.DEBUG:print('Connectionmanager: Node at %r with id %r ignored' % (new_node.getRemoteAddress(), str(new_node.getRemoteId())))
                

                
    def setup_incoming_connection(self):
        """Epoll loop uses this to setup new nodes or clients that connected to us"""
        
        if constants.DEBUG:print('incoming connection')
        #Someone connected to us
        connection, addr = self.serversocket.accept()

        data = tools.recv_msg(connection)
        if data != None:
            data = data.decode('utf8')
            
            
        if data != None and data != '' and data.split(' ')[0] == 'client':
            """Expect client ID"""
            
            node_type = data.split(' ')[0]
            
            if node_type == 'client':
                
                self.epoll.register(connection.fileno(), select.EPOLLIN | select.EPOLLOUT | select.POLLERR | select.EPOLLHUP)
                self.connections[connection.fileno()] = connection
                node_id = data.split(' ')[1]
                self.my_node.addClient(node_id, client_address = addr[0], connection_socket = connection)
                """Update other nodes about this client here"""
                message = {u"from": str(self.my_id), u"to": 'broadcast', 
                           u"message_type": 'update.client.one', u"client": node_id}
                message = json.dumps(message)
                self.send_message_to_all_connected_nodes_internal_use(message)
                """Done, now we can tell the client that it is good to go"""
                #tools.send_msg(connection, b'OK')
                connection.setblocking(0)
                
                outgoing_message_queues[connection] = queue.Queue()
                outgoing_message_queues[connection].put_nowait('OK')
                if constants.DEBUG:print("Connectionmanager: Client connected with id'",node_id)
        elif data != None and data != '' and data.split(' ')[0] == 'node':
            """This is a node that has connected to us"""
            
            node_id = data.split(' ')[1]
            self.epoll.register(connection.fileno(), select.EPOLLIN | select.EPOLLOUT | select.POLLERR | select.EPOLLHUP)
            self.connections[connection.fileno()] = connection
            node_address = addr[0]
            tools.send_msg(connection, b'OK')
            connection.setblocking(0)
            """Add socket so that epoll loop can find and poll it"""
            outgoing_message_queues[connection] = queue.Queue()
            new_node = node.Node(node_id, node_address, connection_socket = connection)
            nodes.update({node_id:new_node})
            self.send_all_clients(connection, node_id)
            
            if constants.DEBUG:print("Connectionmanager: Node (%s, %s) connected" % addr)
            if constants.DEBUG:print('with id',node_id)
        else:
            
            
            connection.close()
            del connection
            
    def handle_incoming_message(self, message, socket):
        """This is where epoll loop goes with new incoming messages, it checks if the message
        is for some internal function or if it is intended to be delivered"""
        
        message = message.decode('utf8')
        if constants.DEBUG:print('Connectionamanger: Got message:',message)
        socket = socket
        try:
            user_message = re.findall(r'<user_message>(.+?)</user_message>',message)[0]
            message = re.sub("<user_message>(.*?)</user_message>", "", message)
            if constants.DEBUG:print('New message with user removed:',message)
        except:
            pass
        message = json.loads(message)
        message_type = message['message_type']

        if message_type == 'update.client.all':
            """The node is telling is what clients it has, and what channels they
            subscribe to"""
            clients = message['clients']
            if constants.DEBUG:print('Connectionmanager: client update')
            from_node = message['from']
            
            """Get this node object"""
            originating_node = nodes.get(from_node)
            for client, client_subscriptions in clients.items():
                originating_node.addClient(client, from_node, socket)
                for channel in client_subscriptions:
                    originating_node.subscribeClient(client, channel)
                    if constants.DEBUG:print('Connectionmanager, subscribed', client, 'to', channel)
         
        if message_type == 'update.client.one':
            client = message['client']
            if constants.DEBUG:print('Connectionmanager: client update one')
            from_node = message['from']
            originating_node = nodes.get(from_node)
            originating_node.addClient(client, from_node, socket)
            if constants.DEBUG:print('Connectionmanager, added, client',client)
                        
        
        if message_type == 'update.client.rip':
            """A node is telling us it has lost a client. This client should be removed
            so we do not send messages to the node if the client was subscribed to any channels"""
            client = message['client']
            from_node = message['from']
            originating_node = nodes.get(from_node)
            if originating_node.removeClient(client) == True:
                if constants.DEBUG:print('Connectionmanager: Removed client',client,'on order from',from_node)
        
        if message_type == 'command.client.subscribeme':
            from_client = message['from']
            channel = message['channel']
            if self.my_node.subscribeClient(from_client, channel) == True:
                if constants.DEBUG:print('Connectionmanager: subscribed client',from_client,'to',channel,
                                         'on order from client')
                message = {u"from": str(self.my_id), u"to": 'broadcast', 
                           u"message_type": 'command.client.subscribe', u"client": from_client, u"channel": channel}
                message = json.dumps(message)
                self.send_message_to_all_connected_nodes_internal_use(message)
                outgoing_message_queues[socket].put_nowait('OK') 
        
        if message_type == 'command.client.subscribe':
            from_node = message['from']
            channel = message['channel']
            client = message['client']
            originating_node = nodes.get(from_node)
            if originating_node.subscribeClient(client, channel) == True:
                if constants.DEBUG:print('Connectionmanager: subscribed client',client,'to',channel,
                                         'on order from', from_node)                        

        if message_type == 'command.client.unsubscribeme':
            from_client = message['from']
            channel = message['channel']
            if self.my_node.unSubscribeClient(from_client, channel) == True:
                if constants.DEBUG:print('Connectionmanager: unsubscribed client',from_client,'from',channel,
                                         'on order from client')
                message = {u"from": str(self.my_id), u"to": 'broadcast', 
                           u"message_type": 'command.client.unsubscribe', u"client": from_client, u"channel": channel}
                message = json.dumps(message)
                self.send_message_to_all_connected_nodes_internal_use(message)
                outgoing_message_queues[socket].put_nowait('OK')         

        if message_type == 'command.client.unsubscribe':
            from_node = message['from']
            channel = message['channel']
            client = message['client']
            originating_node = nodes.get(from_node)
            if originating_node.unSubscribeClient(client, channel) == True:
                if constants.DEBUG:print('Connectionmanager: unsubscribed client',client,'from',channel,
                                         'on order from', from_node)                 
    
        if message_type == 'command.message.channel':
            from_node = message['from']
            channel = message['channel']
            self.send_message_to_channel(from_node, channel, user_message, message_type, message)
            #outgoing_message_queues[socket].put_nowait('OK')
    
        if message_type == 'command.message.channel.internal':
            from_node = message['from']
            channel = message['channel']
            message = json.dumps(message)
            message = message+'<user_message>'+user_message+'</user_message>'
            
            subscribed_clients = self.my_node.getSubscribedClients(channel)
            for client in subscribed_clients:
                client_socket = client.getSocket()
                if client_socket != None:
                    outgoing_message_queues[client_socket].put_nowait(message)        
    
        if message_type == 'command.message.client':
            from_client = message['from']
            to_client = message['to']
            self.send_message_to_client(from_client, to_client, user_message, message_type, message)
            
        if message_type == 'command.message.client.internal':
            from_client = message['from']
            to_client = message['to']
            clients = self.my_node.getClients()
            if to_client in clients:
                to_client = clients.get(to_client)
                client_socket = to_client.getSocket()
                outgoing_message_queues[client_socket].put_nowait(user_message)                   


    def send_message_to_client(self, from_client, to_client, user_message, message_type, message):
        """Sends message to client with channel_id, takes channel as string, string message"""
        """TODO: make sure that message is not delivered to originating client"""
        
        if message_type == 'command.message.client':
            """First check if target
            is a local client"""
            local_clients = self.my_node.getClients()
            if to_client in local_clients:
                to_client = local_clients.get(to_client)
                client_socket = to_client.getSocket()
                outgoing_message_queues[client_socket].put_nowait(user_message)   
            else:
                message['message_type'] = 'command.message.client.internal'
                message = json.dumps(message)
                message = message+'<user_message>'+user_message+'</user_message>'
                for node_id, node_obj in nodes.items():
                    clients = node_obj.getClients()
                    if to_client in clients:
                        to_client = clients.get(to_client)
                        client_socket = to_client.getSocket()
                        outgoing_message_queues[client_socket].put_nowait(message)   
    
    
    
    def send_message_to_channel(self, from_node, channel, user_message, message_type, message):
        """Sends message to channel with channel_id, takes channel as string, string message"""
        """TODO: make sure that message is not delivered to originating client"""
        
        if message_type == 'command.message.channel':
            """Pass this on to every node that has a client subscribed to the channel, and to
            our internal clients that are subscribed"""
            channel_message = json.dumps(message)
            channel_message = channel_message+'<user_message>'+user_message+'</user_message>'
            subscribed_clients = self.my_node.getSubscribedClients(channel)
            for client in subscribed_clients:
                client_socket = client.getSocket()
                if client_socket != None:
                    outgoing_message_queues[client_socket].put_nowait(channel_message)
                
            """Send to nodes that has subscribed clients to this channel, but change the message so it says"""
            message['message_type'] = 'command.message.channel.internal'
            message = json.dumps(message)
            
            message = message+'<user_message>'+user_message+'</user_message>'
            
            for node_id, node_obj in nodes.items():
                subscribed_clients = node_obj.getSubscribedClients(channel)
                if len(subscribed_clients) != 0:
                    outgoing_message_queues[node_obj.getConnectionSocket()].put_nowait(message)
                    
    
    
    
    def send_all_clients(self, socket, target_id):
        my_clients = self.my_node.getClients()
        dict_for_sending = {}
        for client_id, client_obj in my_clients.items():
            subscription_list = client_obj.get_subscriptions()
            dict_for_sending.update({client_id:subscription_list})
        message = {u"from": str(self.my_id), u"to": str(target_id), 
        u"message_type": 'update.client.all', u"clients": dict_for_sending}
        message = json.dumps(message)
        outgoing_message_queues[socket].put_nowait(message)
        
            
        
    def beacon_listener(self,my_id,my_address):
        """Listens for broadcasts from other nodes and places them in epoll queue to be added"""
        
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        try:
            s.bind(('0.0.0.0', 6300))
        except:
            print('failure to bind')
            s.close()
            raise
        while run:
            message = s.recv(constants.BUFSIZE)
            message = message.decode('utf8')
            
            beaconid, beaconport, beaconaddress = message.split(' ')
            beaconid = uuid.UUID(beaconid)
            #if constants.DEBUG:print('Connectionmanager: Found peer at %r, with id: %r and port: %r' % (beaconaddress, beaconid, beaconport))
            if (beaconid != self.my_id and beaconid < self.my_id and str(beaconid) not in nodes):
                new_peer = node.Node(str(beaconid), beaconaddress, beaconport)

                nodes.update({str(beaconid):''})
                add_to_epoll_queue.put_nowait(new_peer)        
        
def main():
    config = configparser.ConfigParser()
    
    config.read('/etc/dcass/dcass.conf')
    
    #print('Configfile error')
    hostaddress = config['network']['hostaddress']
    networkaddress = config['network']['networkaddress']
    cm_id = config['id']['node_id']
    debug = config['debug'].getboolean('debug')
    print('Configoptions:', hostaddress, networkaddress, cm_id, debug)
    cm = ConnectionManager(hostaddress, networkaddress, debug)
    cm.run()

if __name__ == '__main__':
    main()      
        
        