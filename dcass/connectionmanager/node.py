#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  Node.py
#  
#  Copyright 2013 Per Karlsson, pkarl.home@gmail.com
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

class Node:

    def __init__(self, node_id, remote_address=None, remote_port=None, connection_socket=None):
        self.__node_id = node_id
        self.__remote_address = remote_address
        self.__remote_port = remote_port
        self.__connection_socket = connection_socket
        self.clients = {}

    def getNodeId(self):
        return self.__node_id


    def getRemoteAddress(self):
        return self.__remote_address


    def getRemotePort(self):
        return self.__remote_port

    def getConnectionSocket(self):
        return self.__connection_socket

    
    def setNodeId(self, value):
        self.__node_id = value


    def setRemoteAddress(self, value):
        self.__remote_address = value


    def setRemotePort(self, value):
        self.__remote_port = value

    def setConnectionSocket(self, value):
        self.__connection_socket = value
             
    def addClient(self, client_id, client_address, connection_socket=None):
        client_obj = Client(client_id, client_address, connection_socket)
        self.clients.update({client_id:client_obj})
    
    def removeClient(self, client_id):
        if client_id in self.clients:
            self.clients.pop(client_id, None)
            return True
        else:
            return False
        
    def getClients(self):
        return self.clients
    
    def getSubscribedClients(self, channel):
        subscribed_clients = []
        for client in self.clients.values():
            channels = client.get_subscriptions()
            for subscribed_channel in channels:
                if subscribed_channel == channel:
                    subscribed_clients.append(client)
        return subscribed_clients
                    
    
    
    
    def subscribeClient(self, client_id, channel_name):
        client = self.clients.get(client_id)
        if client != None:
            client.subscribe(channel_name)
            return True
        else:
            return False
        
    def unSubscribeClient(self, client_id, channel_name):
        client = self.clients.get(client_id)
        if client != None:
            client.unsubscribe(channel_name)
            return True
        else:
            return False

    
class Client(object):

    def __init__(self, client_id, client_address, connection_socket=None):
        """If client socket is None, this is not a local client to this node"""
        self.client_id = client_id
        self.client_address = client_address
        self.connection_socket = connection_socket
        self.channels = []
        
    def get_client_id(self):
        return self.client_id
    
    def get_subscriptions(self):
        return self.channels
    
    def subscribe(self, channel_name):
        self.channels.append(channel_name)
    
    def unsubscribe(self, channel_name):
        if len(self.channels) != 0:
            try:
                self.channels.remove(channel_name)
            except:
                print('Bad channel name')
                
    def getSocket(self):
        return self.connection_socket