#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  cmclient.py
#  
#  Copyright 2013 Per Karlsson, pkarl.home@gmail.com
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import socket
import sys
from util import tools as tools
from constants import constants
import simplejson as json
import select
import queue
import threading
import time
import re
class Cmclient(object):

    
    '''
    Helper class to connect to connection manager
    '''


    def __init__(self, my_client_id, cm_address):
        self.my_client_id = str(my_client_id)
        self.cm_address = cm_address
        self.channels = {}
        self.outqueue = queue.Queue()
        self.inqueue = queue.Queue()
        self.commandqueue = queue.Queue()
        self.commandresponsequeue = queue.Queue()
        self.timeout_in_seconds = 0.001
        self.t1 = threading.Thread(target=self.postman);
        self.t1.daemon = True
        
    def connect_to_cm(self):
        attempts = 0
        while True:
            try:
                attempts += 1
                self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.sock.connect((self.cm_address, int(4501)))
                self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                self.sock.setsockopt(socket.SOL_TCP, socket.TCP_NODELAY, 1)
                message = 'client '+str(self.my_client_id)
                tools.send_msg(self.sock, message.encode('utf8'))
                response = tools.recv_msg(self.sock)
                
                if response.decode('utf8') == 'OK':
                    print('Client', self.my_client_id, 'connected to cm')
                    
                    if self.t1.is_alive() != True:
                        
                        self.t1.start()
                    return True
                else:
                    
                    continue
            except KeyboardInterrupt:
                print('Ok, giving up.')
                sys.exit(0)
            
            except (ConnectionError, ConnectionRefusedError):
                if attempts == 1:
                    print('Cant connect to connection manager. Is it running? Retrying... Ctrl-c to give up.')
                time.sleep(1)
                continue
        
    def rejoin_channels(self):
        for channel in self.channels:
            self.join_channel(channel)

    
    def join_channel(self, channel):
        """TODO: Fix sorting mechanism so that messages to channels end up in
        different queues. This way, if we need an answer to some message, we
        can specify a reply channel that we join, and hang on that queue until
        an answer comes in or it times out. That should ensure that the message
        we get is really the message we were waiting for, not some other message
        for something else."""
        
        self.channel = channel
        message = {u"channel": channel, u"message_type": 'command.client.subscribeme', 
                   u'from' :self.my_client_id}
        message = json.dumps(message)
        self.send_command(str(message).encode('utf8'))
        
        response = self.get_command_status()
        if response == 'OK':
            
            if channel not in self.channels:
                channel_queue = queue.Queue()
                self.channels.update({channel:channel_queue})
            return True
        if not response:
            """Command failed"""
            return False
        
    def leave_channel(self, channel):
        self.channel = channel
        message = {u"channel": channel, u"message_type": 'command.client.unsubscribeme', 
                   u'from' :self.my_client_id}
        message = json.dumps(message)
        tools.send_msg(self.sock, str(message).encode('utf8'))
        response = self.get_command_status()
        if response == 'OK':
            if channel in self.channels:
                self.channels.pop(channel)
                return True
        if not response:
            """We lost the connection."""        
        
    def send_message_to_channel(self,channel, message, from_client):
        
        msg = {u"channel": channel, 
        u"message_type": 'command.message.channel', u"from": from_client}
        msg = json.dumps(msg)
        msg = msg+'<user_message>'+message+'</user_message>'
        try:
            self.send_message(str(msg).encode('utf8'))
            #tools.send_msg(self.sock, msg.encode('utf8')) 
            return True
        except:
            return False
            
    def send_message_to_client(self, to_client, from_client, message):
        msg = {u"to": to_client, 
        u"message_type": 'command.message.client', u"from": from_client}
        msg = json.dumps(msg)
        msg = msg+'<user_message>'+message+'</user_message>'
        
            
        self.send_message(str(msg).encode('utf8'))
        #tools.send_msg(self.sock, msg.encode('utf8'))    
        return True
    
    
    
    def get_message(self):
        try:
            msg = self.inqueue.get(True, 2)
            return msg
        except queue.Empty:
            return None
    
    def get_message_count(self):
        msg_count = self.inqueue.qsize()
        return msg_count    
    
    def get_channel_message(self,channel):
        if channel in self.channels.keys():
            chn_queue = self.channels.get(channel)
            try:
                channel_message = chn_queue.get(True, 2)
                return channel_message
            except queue.Empty:
                return None
    
    def get_channel_message_count(self,channel):
        if channel in self.channels.keys():
            chn_queue = self.channels.get(channel)
            channel_message_count = chn_queue.qsize()
            return channel_message_count
    
    
    
    def get_command_status(self):
        try:
            msg = self.commandresponsequeue.get(True, 2)
            
            return msg    
        except queue.Empty:
            return False

    def send_message(self, msg):
        self.outqueue.put_nowait(msg)
            
    def send_command(self,msg):
        #print('trying to put msg:',msg)
        self.outqueue.put_nowait(msg)
    
    def postman(self):
        time.sleep(0.1)
        self.sock.setblocking(0)
        input_socket = [self.sock]
        output_socket = [self.sock]
        error_socket = [self.sock]
        while True:
            #print('in select')
            readable, writable, exceptional = select.select(input_socket, output_socket, error_socket, self.timeout_in_seconds)
            for s in writable:
                #print('test')
                try:
                    msg = self.outqueue.get_nowait()
                    
                except queue.Empty:
                    #print('outqueue empty')
                    output_socket.remove(self.sock)
                else:
                    msg = tools.send_msg(self.sock, msg)
            
            for s in readable:
                msg = tools.recv_msg(self.sock)
                if msg == None or len(msg) == 0:
                    
                    self.sock.close()

                    if self.connect_to_cm() == True:
                        input_socket = [self.sock]
                        output_socket = [self.sock]
                        error_socket = [self.sock]
                        self.t2 = threading.Thread(target=self.rejoin_channels);
                        self.t2.daemon = True
                        self.t2.start()
                        """make sure we join the channels we need"""
                else:
                    try:
                        msg = msg.decode('utf8')
                        if msg == 'OK':
                            self.commandresponsequeue.put_nowait(msg)
                        else:
                            try:
                                
                                user_message = re.findall(r'<user_message>(.+?)</user_message>',msg)[0]
                                message = re.sub("<user_message>(.*?)</user_message>", "", msg)
                                message = json.loads(message)
                                channel = message['channel']
                                if channel in self.channels.keys():
                                    chn_queue = self.channels.get(channel)
                                    chn_queue.put_nowait(user_message)
                                    
                            except:
                                pass
                            
                            self.inqueue.put_nowait(msg)
                    except:
                        pass

            if not (readable or writable or exceptional):
                pass
            try:
                msg = self.outqueue.get_nowait()
                
            except queue.Empty:
                pass
            else:
                self.outqueue.put_nowait(msg)
                output_socket.append(self.sock)
            

            
            
            