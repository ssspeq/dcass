#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  beacon.py
#  
#  Copyright 2013 Per Karlsson, pkarl.home@gmail.com
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import socket
import threading
import time
import random


s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
Run = True

class Beacon: 


    def __init__(self,my_id,my_port,net_address, my_address):
        self.net_address = net_address
        
        self.my_port = my_port
        self.my_id = my_id
        self.my_address = my_address
        
    def run(self):
        t1 = threading.Thread(target=self.startBeacon)
        t1.daemon = True
        t1.start()
    
    def stop(self):
        Run = False    

    def startBeacon(self):
        randomDelay = random.uniform(1,4)
        while Run:
            message = self.my_id+" "+self.my_port+" "+self.my_address
            message = message.encode('utf-8')
            s.sendto(message, (self.net_address, 6300))
            time.sleep(1)
