#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  models.py
#  
#  Copyright 2013 Per Karlsson, pkarl.home@gmail.com
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#

from sqlalchemy import Column, ForeignKey, Integer, String, create_engine, Table
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref, sessionmaker
from sqlalchemy.types import TypeDecorator, CHAR
from sqlalchemy.dialects.postgresql import UUID
import uuid

Base = declarative_base()
 
class GUID(TypeDecorator):
    """Platform-independent GUID type.

    Uses Postgresql's UUID type, otherwise uses
    CHAR(32), storing as stringified hex values.

    """
    impl = CHAR

    def load_dialect_impl(self, dialect):
        if dialect.name == 'postgresql':
            return dialect.type_descriptor(UUID())
        else:
            return dialect.type_descriptor(CHAR(32))

    def process_bind_param(self, value, dialect):
        if value is None:
            return value
        elif dialect.name == 'postgresql':
            return str(value)
        else:
            if not isinstance(value, uuid.UUID):
                return "%.32x" % uuid.UUID(value)
            else:
                # hexstring
                return "%.32x" % value

    def process_result_value(self, value, dialect):
        if value is None:
            return value
        else:
            return uuid.UUID(value)

class User(Base):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True)
    name = Column(String(250), nullable=False, unique=True)
    password = Column(String(50), nullable=False)
    
class Command(Base):
    __tablename__ = 'commands'
    id = Column(Integer, primary_key=True)
    name = Column(String(50), nullable=False, unique=True)
    def __init__(self, name):
        """"""
        self.name = name 
        
class Category(Base):
    __tablename__ = 'categories'
    id = Column(Integer, primary_key=True)
    name = Column(String(50), nullable=False, unique=True)
    def __init__(self, name):
        """"""
        self.name = name 

device_command_association_table = Table('device_command_association', Base.metadata,
    Column('device', Integer, ForeignKey('devices.id')),
    Column('command', Integer, ForeignKey('commands.id'))
)

device_definition_device_association_table = Table('device_definition_device_association', Base.metadata,
    Column('device', Integer, ForeignKey('devices.id')),
    Column('device_definition', Integer, ForeignKey('device_definitions.id'))
)

class Device_definition(Base):
    __tablename__ = 'device_definitions'
    id = Column(Integer, primary_key=True)

    name = Column(String(250), nullable=False, unique=True)
    description = Column(String(250))
    category_id = Column(Integer, ForeignKey('categories.id'))
    module_name = Column(String(250))
    category = relationship("Category",backref=backref("device_definitions", order_by=id))
    devices = relationship("Device",
                    secondary=device_definition_device_association_table,
                    backref="device_definitions")

class Device(Base):
    __tablename__ = 'devices'
    id = Column(Integer, primary_key=True)
    name = Column(String(250), nullable=False, unique=True)
    description = Column(String(250))
    category_id = Column(Integer, ForeignKey('categories.id'))
    category = relationship("Category",backref=backref("devices", order_by=id))
    commands = relationship("Command",
                    secondary=device_command_association_table,
                    backref="devices")


class Configured_device(Base):
    __tablename__ = 'configured_devices'
    id = Column(Integer, primary_key=True)
    universal_id = Column(GUID, nullable=False, unique=True)
    name = Column(String(250), nullable=False, unique=True)
    description = Column(String(250))
    device_definition_id = Column(Integer, ForeignKey('device_definitions.id'))    
    device_definition = relationship("Device_definition")

    
if __name__ == '__main__':    
    engine = create_engine('postgresql://postgres:password@localhost:5432/dcass')
    Base.metadata.create_all(engine)
    DBSession = sessionmaker(bind=engine)
    
    session = DBSession()
    #admin_user = session.query(User).filter(User.name == 'admin').first()
    admin_user = User(name='admin', password='admin')
    session.add(admin_user)
    session.commit()
    session.add_all([Category(name = "control"),
                     Category(name = "media"),
                     Category(name = "security"),
                     Category(name = "system")
                     ])    
    session.add_all([Command(name = "on"),
                     Command(name = "off"),
                     Command(name = "allon"),
                     Command(name = "alloff")
                     ])    
    session.commit()
    testdevice = Device(name = 'switch', description = 'A switch with states on, off')
    command = session.query(Command).filter(Command.name == 'on').first()
    testdevice.commands.append(command)
    command = session.query(Command).filter(Command.name == 'off').first()
    testdevice.commands.append(command)
    new_category = session.query(Category).filter(Category.name == 'control').first()
    testdevice.category = new_category
    session.add(testdevice)
    session.commit()
    testdevice = Device(name = 'digitalinput', description = 'Input with states high, low')
    session.add(testdevice)
    session.commit()
    test_device = Device_definition(name = 'Testdevice', description = 'Device for testing',
                                    module_name = 'testdevice.py')
    session.add(test_device)
    session.commit()

    new_category = session.query(Category).filter(Category.name == 'control').first()
    test_device = session.query(Device_definition).filter(Device_definition.name == 'Testdevice').first()

    test_device.category = new_category
    session.add(test_device)
    session.commit()
    
    """Add device manager and other system devices"""
    dmanager = Device_definition(name = 'Devicemanager', description = 'System device that handles all devices',
                                    module_name = 'dmanager.py')
    session.add(dmanager)
    session.commit() 
    new_category = session.query(Category).filter(Category.name == 'system').first()
    dmanager = session.query(Device_definition).filter(Device_definition.name == 'Devicemanager').first()

    dmanager.category = new_category
    session.add(dmanager)
    session.commit()       
    
    child_device = session.query(Device).filter(Device.name == 'switch').first()
    test_device.devices.append(child_device)
    child_device = session.query(Device).filter(Device.name == 'digitalinput').first()
    test_device.devices.append(child_device)
    session.add(test_device)
    session.commit()
    my_device = Configured_device(name = 'Test', description = 'System test device', universal_id = uuid.uuid4() )
    device_to_link = session.query(Device_definition).filter(Device_definition.name == 'Testdevice').first()
    my_device.device_definition = device_to_link
    session.add(my_device)
    session.commit()
    my_device = Configured_device(name = 'Devicemanager', description = 'System device that handles all devices', universal_id = uuid.uuid4() )
    device_to_link = session.query(Device_definition).filter(Device_definition.name == 'Devicemanager').first()
    my_device.device_definition = device_to_link
    session.add(my_device)
    session.commit()    
