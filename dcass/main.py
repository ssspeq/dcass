#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  main.py
#  
#  Copyright 2013 Per Karlsson, pkarl.home@gmail.com
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

# This program starts connectionmanager and devicemanager.

import subprocess
import time
import configparser
import uuid
import sys
import simplejson as json
run = True
pids = {} #This is a list of devices we started
devices = []
def main():

    run_devices()

  
    while True:
        time.sleep(1)
        if len(pids) != 0:
            for pid in pids.values():
                stdoutdata, stderrdata = pid.communicate()
                print(stdoutdata, stderrdata)
                if pid.returncode:
                    print('Ohoh, crash.')
                    
    return 0

def run_devices():
    global devices
    config = configparser.ConfigParser()
    config.read('/etc/dcass/dcass.conf')
    node_id = config['id']['node_id']
    if node_id == '':
        node_id = str(uuid.uuid4())
        print('Generated unique ID for connection manager:', node_id)
        config.set('id','node_id',node_id)
    fp = open('/etc/dcass/dcass.conf', 'w')
    config.write(fp)
    fp.close()
    print('Saved configuration file.')
    try:
        with open('/etc/dcass/devices/devices.json' , 'r') as infile:
            devices = json.load(infile)
    except:
        print('Deviceconfig file error.')
        #Generate UUID for our local devicemanager
        devicemanager = {}
        devicemanager.update({'name':'devicemanager'})
        devicemanager.update({'uuid':str(uuid.uuid4())})
        devicemanager.update({'device_file':'./devices/devicemanager/dmanager.py'})
        
        devices.append(devicemanager)
        with open('/etc/dcass/devices/devices.json' , 'w') as outfile:
            json.dump(devices, outfile)
  

    pid = subprocess.Popen(["./connectionmanager/cmanager.py"])
    stdoutdata, stderrdata = pid.communicate()
    if pid.returncode:
        """FAIL"""
        print(pid.returncode)
        time.sleep(5)
    else:
        pids.update({'cm': pid})
        return pids 




if __name__ == '__main__':
    main()
