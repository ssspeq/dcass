#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  dmanager.py
#  
#  Copyright 2013 Per Karlsson, pkarl.home@gmail.com
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#

import simplejson as json
import argparse
import util.messaging
import uuid
import time
import configparser
import subprocess

parser=argparse.ArgumentParser()
parser.add_argument('-a', help = 'Network address, X.X.X.X', required=True)
parser.add_argument('-id', help = 'UUID of this node')
args=parser.parse_args()
my_id = args.id
my_reply_channel = uuid.uuid4()
cm_address = args.a
messenger = util.messaging.Messenger(my_id, cm_address)
messenger.connect()
pids = {} #This is a list of devices we started
devices = []

def main():
    messenger.join_channel('devicemanager')
    messenger.join_channel('events')
    run_devices()
    while 1:
        if messenger.get_message_count() != 0:
            message = messenger.get_message()
            print(message)
            if message != None:
                check_message(message)
        if messenger.get_channel_message_count('devicemanager') != 0:
            message = messenger.get_channel_message('devicemanager')
            print(message)
            if message != None:
                check_message(message)
        if messenger.get_channel_message_count('events') != 0:
            message = messenger.get_channel_message('events')
            print(message)
            if message != None:
                check_message(message)
        
        time.sleep(0.001)

def check_message(message):
    if message != None and message["subject"] == "event.device.announce":
            print('Someone is reporting in a device')
            print('Message contents:', message["content"])
    


def run_devices():
    global devices
    
    devices = load_device_map()
    print(devices)

def save_device_map(devices):
    with open('/etc/dcass/devices/devices.json' , 'w') as outfile:
        json.dump(devices, outfile)

def load_device_map():
    with open('/etc/dcass/devices/devices.json' , 'r') as infile:
        devices = json.load(infile)
        return devices
    
def handle_event(message):
    pass

if __name__ == '__main__':
    
    main()