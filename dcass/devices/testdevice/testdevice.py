#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  testdevice.py
#  
#  Copyright 2013 Per Karlsson, pkarl.home@gmail.com
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#


import argparse
import util.messaging
import subprocess
import uuid
import threading
import time


"""A selfcontained unit started by devicemanager that handles and controls
some devices"""

parser=argparse.ArgumentParser()
parser.add_argument('-a', help = 'Network address, X.X.X.X', required=True)
parser.add_argument('-id', help = 'UUID of this node')
args=parser.parse_args()
my_id = args.id
my_reply_channel = uuid.uuid4()
cm_address = args.a
messenger = util.messaging.Messenger(my_id, cm_address)
messenger.connect()


        
def command_handler(internal_id, content):
    
    
    if internal_id == "output_1" and content["command"] == "on":
        print('Output 1 active')
        subprocess.call(['paplay', '/usr/share/sounds/purple/receive.wav'])
        return 'OK'
        
    elif internal_id == "output_1" and content["command"] == "off":
        subprocess.call(['paplay', '/usr/share/sounds/purple/send.wav'])
        return 'OK'

    elif internal_id == "output_2" and content["command"] == "off":
        subprocess.call(['paplay', '/usr/share/sounds/purple/send.wav'])
        return 'OK'
    
    elif internal_id == "output_2" and content["command"] == "on":
        print('Output 1 active')
        subprocess.call(['paplay', '/usr/share/sounds/purple/receive.wav'])
        return 'OK'


class testEvent(threading.Thread):
    def __init__(self,):
        threading.Thread.__init__(self)    
    def run(self):
        level = 0
        while (True):
            messenger.send_event("digital_sensor", "event.security.sensortriggered", level, "")
            if (level == 0):
                level = 255
            else:
                level = 0
            time.sleep (5)

def main():
    messenger.add_device("output_1", "test device")
    messenger.add_device("output_2", "test device")
    messenger.add_device("digital_sensor", "binarysensor")
    messenger.register_command_handler(command_handler)
    

      
    background = testEvent()
    background.setDaemon(True)
    background.start()
    messenger.run()

if __name__ == '__main__':
    main()
