This is the home of DCASS, Distributed control and security system

What is DCASS?

It is (or will be) a distributed system for control and security purposes. It could be used to control things,
as a regular alarm system or as a fullblown security system. By that, I mean the kind that is used in prisons,
banks, or other such places. The term distributed here means that the system should be able to fully work with
only one node, as well as with hundreds. For this to work, the common roles of "client" and "server" must be
of somewhat diminished importance. One node must in itself, be a fully working and complete system and when
more nodes are introduced, they will cooperate and form a bigger system.

What is working?

There is a quite crude communication system in place, called connection manager. This supplys communication
channels to the nodes. They can be addressed directly or via channels. Connection manager is not a server per
se, it merely connects all nodes to a kind of a mesh network, where all nodes are a kind of server. It uses
broadcast messages for the nodes to be able to find eachother. This allows all nodes to connect to eachother,
and every connectionmanager instance also allows clients to connect to them and access the entire network. In
this manner, no singe node can bring down the system, and it is fully functional even if only one node remains.
(Of course, the nodes that are gone cannot be communicated with, but the one remaining node will still work and
supply communication to its clients.) 

There is a simple communication protocoll based on json. Using this you can send commands to devices in the
system, on, off, or anything really. This works in a similar way as agocontrol (a pretty cool control system,
check it out if you havent already). 

What is missing?

A device manager device: 
(Devices are pretty much just programs connected to connection manager and DCASS) Device
manager will handle devices, make sure they are running, restart them if they for any reason should go down,
make it possible to add, change and delete devices. Devicemanager will also maintain a table of devices, their
child devices and the addresses of every device, and trigger alarms if devices go missing in action. Every device
that is controlled by some other device, accepts commands via its parent device. There must therefore be some kind
of lookup function here, so child devices can be found and addressed.

An event manager device:
This device will listen for events sent by devices and make sure actions are performed if there are any programmed.

A job manager device:
This device will run jobs as its name suggests. A job is a bundle of commands, things that you want to happen when
a certain event occurs. This kind of job bundles are commonly called scenarios.

A time device:
This device will send events based on time. Seconds, minutes, etc.

Some kind of interface:
A interface for system control, intended for users. If this system is supposed to be a proper security system, there
is alot of work to do here. It must support multiple instances of the control interface, user and access control,
a management interface for alarms and events, (they must be sent to all instances, support acknowledge and reset of
alarms etc.) There must also be some way for this interface to connect to the system. Since there is no server per
se, the interface should be able to just connect to any node and still access the entire system.

Other things:
Timechannels (Basically a global variable that becomes true or false depending on time or date) so that functions
can be programmed on schedual. Alarms for communication errors, missing devices and so on. 

If this system is to work the way I intended it to, a node (basically a device running an instance of connection
manager that lets other devices run on it) must have all of the above system devices running, in fact EVERY node
must. Here is the tricky part: How to get all device managers to seamlessly cooperate? How to make sure that a job
triggered by some event only runs once, even though there culd be a bunch of event managers picking up the event
and wanting to run the job? Some suggestions could be: Implement some kind of joboffer system. A device that sends
an event because something happened, maybe an input changed status, offers this event "on the market" to all event
managers. Then, they would all bid for it, maybe the event manager on the least busy node will get it. In this way,
it would be guaranteed to be triggered once, only once, and no more than once. Event manager, if there are any jobs
programmed for this event, would then do the same and offer it on the market to jobmanager devices. Distributed
programming is HARD, and this is only a suggestion. Hopefully, someone has other better suggestions on how to solve
this.

I am probably forgetting a thousand things as I am writing this. The documentation for this project is somewhat
nonexistant, I will try to fix that as soon as time permits.
